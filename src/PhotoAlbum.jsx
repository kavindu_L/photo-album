import { useState, useEffect } from "react";

import ImageThumbnailList from "./ImageThumbnailList";
import FullImage from "./FullImage";
import "./index.css";

function PhotoAlbum() {
  const [fetchedImageList, setFetchedImageList] = useState([]);
  const [mainImage, setMainImage] = useState({});
  // const [show, setShow] = useState(true);

  function handlePrevious() {
    // console.log(mainImage.id - 1);
    if (mainImage.id !== fetchedImageList[0].id) {
      setMainImage(
        currState =>
          fetchedImageList.filter(
            img => Number(img.id) === Number(currState.id) - 1
          )[0]
      );
    }
  }

  function handleNext() {
    if (mainImage.id !== fetchedImageList[fetchedImageList.length - 1].id) {
      setMainImage(
        currState =>
          fetchedImageList.filter(
            img => Number(img.id) === Number(currState.id) + 1
          )[0]
      );
    }
  }

  function changeMainImage(event) {
    console.log(Number(event.target.id));
    // console.log(
    //   fetchedImageList.filter(img => img.id === Number(event.target.id))[0]
    // );

    setMainImage(
      fetchedImageList.filter(
        img => Number(img.id) === Number(event.target.id)
      )[0]
    );
    // console.log(mainImage);
  }

  useEffect(() => {
    fetch("https://picsum.photos/v2/list")
      .then(res => res.json())
      .then(data => {
        setFetchedImageList(data.filter(img => img.id < 16));
        // console.log(fetchedImageList[0]);
        // setMainImage(fetchedImageList[0]);
      })
      .catch(err => console.log(err));
  }, []);

  // console.log(typeof mainImage.id);

  return (
    <div className="container mt-5 w-50">
      <h1 className="header-txt mb-3">Photo Album</h1>
      <>
        {/* {!mainImage.id ? (
          <div className="spinner-border" role="status">
            <span className="visually-hidden">Loading...</span>
          </div>
        ) : (
          <FullImage
            image={mainImage}
            handlePrevious={handlePrevious}
            handleNext={handleNext}
            imageList={fetchedImageList}
          />
        )} */}

        {mainImage.id && (
          <FullImage
            image={mainImage}
            handlePrevious={handlePrevious}
            handleNext={handleNext}
            imageList={fetchedImageList}
          />
        )}

        <ImageThumbnailList
          imageList={fetchedImageList}
          changeMainImage={changeMainImage}
          mainImage={mainImage}
        />
      </>

      {/* {mainImage ? (
        <div className="spinner-border" role="status">
          <span className="visually-hidden">Loading...</span>
        </div>
      ) : (
        <FullImage
          image={mainImage}
          handlePrevious={handlePrevious}
          handleNext={handleNext}
          imageList={imageList}
        />
      )}

      {imageList.length === 0 ? (
        <div className="spinner-border" role="status">
          <span className="visually-hidden">Loading...</span>
        </div>
      ) : (
        <ImageThumbnailList
          imageList={imageList}
          changeMainImage={changeMainImage}
          mainImage={mainImage}
        />
      )} */}

      {/* {show ? (
        <div className="spinner-border" role="status">
          <span className="visually-hidden">Loading...</span>
        </div>
      ) : (
        <>
          <FullImage
            image={mainImage}
            handlePrevious={handlePrevious}
            handleNext={handleNext}
            imageList={imageList}
          />
          <ImageThumbnailList
            imageList={imageList}
            changeMainImage={changeMainImage}
            mainImage={mainImage}
          />
        </>
      )} */}
    </div>
  );
}

export default PhotoAlbum;
