import { BsChevronLeft, BsChevronRight } from "react-icons/bs";

function FullImage({ image, handlePrevious, handleNext, imageList }) {
  return (
    <>
      {/* <h3>Full Image</h3> */}
      <div className="d-flex justify-content-center align-items-center justify-content-evenly mt-5 mb-5">
        <div onClick={e => handlePrevious(e)}>
          <BsChevronLeft
            size={70}
            color={imageList[0].id !== image.id ? "black" : "aliceblue"}
          />
        </div>

        <figure className="figure">
          <img
            src={image.url}
            className="figure-img img-fluid rounded"
            alt={image.title}
          />
          <figcaption className="figure-caption text-center">
            {image.title}
          </figcaption>
        </figure>
        <div onClick={handleNext}>
          <BsChevronRight
            size={70}
            color={
              imageList[imageList.length - 1].id !== image.id
                ? "black"
                : "aliceblue"
            }
          />
        </div>
      </div>
    </>
  );
}

export default FullImage;
