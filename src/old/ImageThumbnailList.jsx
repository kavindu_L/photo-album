function ImageThumbnailList({ imageList, changeMainImage, mainImage }) {
  return (
    <>
      {/* <h3>Image thumbnail list</h3> */}
      <div className="d-flex flex-wrap" onClick={changeMainImage}>
        {imageList.map(img => (
          <div
            key={img.id}
            className={
              mainImage.id === img.id
                ? "me-3 mb-3 border border-info border-4 rounded"
                : "me-3 mb-3"
            }
          >
            <img
              src={img.thumbnailUrl}
              className="img-thumbnail rounded"
              alt={img.name}
              id={img.id}
            />
          </div>
        ))}
      </div>
    </>
  );
}

export default ImageThumbnailList;
