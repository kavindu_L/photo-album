import ImageThumbnailList from "./ImageThumbnailList";
import FullImage from "./FullImage";
import { useState, useEffect } from "react";

function PhotoAlbum() {
  // const imageList = [
  //   {
  //     albumId: 1,
  //     id: 1,
  //     title: "accusamus beatae ad facilis cum similique qui sunt",
  //     url: "https://via.placeholder.com/600/92c952",
  //     thumbnailUrl: "https://via.placeholder.com/150/92c952"
  //   },
  //   {
  //     albumId: 1,
  //     id: 2,
  //     title: "reprehenderit est deserunt velit ipsam",
  //     url: "https://via.placeholder.com/600/771796",
  //     thumbnailUrl: "https://via.placeholder.com/150/771796"
  //   },
  //   {
  //     albumId: 1,
  //     id: 3,
  //     title: "officia porro iure quia iusto qui ipsa ut modi",
  //     url: "https://via.placeholder.com/600/24f355",
  //     thumbnailUrl: "https://via.placeholder.com/150/24f355"
  //   },
  //   {
  //     albumId: 1,
  //     id: 4,
  //     title: "culpa odio esse rerum omnis laboriosam voluptate repudiandae",
  //     url: "https://via.placeholder.com/600/d32776",
  //     thumbnailUrl: "https://via.placeholder.com/150/d32776"
  //   },
  //   {
  //     albumId: 1,
  //     id: 5,
  //     title: "natus nisi omnis corporis facere molestiae rerum in",
  //     url: "https://via.placeholder.com/600/f66b97",
  //     thumbnailUrl: "https://via.placeholder.com/150/f66b97"
  //   }
  // ];

  // const image = {
  //   albumId: 1,
  //   id: 1,
  //   title: "accusamus beatae ad facilis cum similique qui sunt",
  //   url: "https://via.placeholder.com/600/92c952",
  //   thumbnailUrl: "https://via.placeholder.com/150/92c952"
  // };

  const [fetchedImageList, setFetchedImageList] = useState([]);
  const [mainImage, setMainImage] = useState({});
  // const [show, setShow] = useState(true);

  function handlePrevious() {
    // console.log(mainImage.id - 1);
    if (mainImage.id !== fetchedImageList[0].id) {
      setMainImage(
        currState =>
          fetchedImageList.filter(img => img.id === currState.id - 1)[0]
      );
    }
  }

  function handleNext() {
    if (mainImage.id !== fetchedImageList[fetchedImageList.length - 1].id) {
      setMainImage(
        currState =>
          fetchedImageList.filter(img => img.id === currState.id + 1)[0]
      );
    }
  }

  function changeMainImage(event) {
    // console.log(typeof Number(event.target.id));
    // console.log(
    //   fetchedImageList.filter(img => img.id === Number(event.target.id))[0]
    // );

    setMainImage(
      fetchedImageList.filter(img => img.id === Number(event.target.id))[0]
    );
    // console.log(mainImage);
  }

  useEffect(() => {
    fetch("https://jsonplaceholder.typicode.com/photos")
      .then(res => res.json())
      .then(data => {
        setFetchedImageList(data.filter(img => img.id < 16));
        // console.log(fetchedImageList[0]);
        // setMainImage(fetchedImageList[0]);
      })
      .catch(err => console.log(err));
  }, []);

  // console.log(typeof mainImage.id);

  return (
    <div className="container mt-3 w-50">
      <h1>Photo Album</h1>
      <>
        {/* {!mainImage.id ? (
          <div className="spinner-border" role="status">
            <span className="visually-hidden">Loading...</span>
          </div>
        ) : (
          <FullImage
            image={mainImage}
            handlePrevious={handlePrevious}
            handleNext={handleNext}
            imageList={fetchedImageList}
          />
        )} */}

        {mainImage.id && (
          <FullImage
            image={mainImage}
            handlePrevious={handlePrevious}
            handleNext={handleNext}
            imageList={fetchedImageList}
          />
        )}

        <ImageThumbnailList
          imageList={fetchedImageList}
          changeMainImage={changeMainImage}
          mainImage={mainImage}
        />
      </>

      {/* {mainImage ? (
        <div className="spinner-border" role="status">
          <span className="visually-hidden">Loading...</span>
        </div>
      ) : (
        <FullImage
          image={mainImage}
          handlePrevious={handlePrevious}
          handleNext={handleNext}
          imageList={imageList}
        />
      )}

      {imageList.length === 0 ? (
        <div className="spinner-border" role="status">
          <span className="visually-hidden">Loading...</span>
        </div>
      ) : (
        <ImageThumbnailList
          imageList={imageList}
          changeMainImage={changeMainImage}
          mainImage={mainImage}
        />
      )} */}

      {/* {show ? (
        <div className="spinner-border" role="status">
          <span className="visually-hidden">Loading...</span>
        </div>
      ) : (
        <>
          <FullImage
            image={mainImage}
            handlePrevious={handlePrevious}
            handleNext={handleNext}
            imageList={imageList}
          />
          <ImageThumbnailList
            imageList={imageList}
            changeMainImage={changeMainImage}
            mainImage={mainImage}
          />
        </>
      )} */}
    </div>
  );
}

export default PhotoAlbum;
