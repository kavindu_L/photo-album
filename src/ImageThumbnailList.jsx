function ImageThumbnailList({ imageList, changeMainImage, mainImage }) {
  return (
    <>
      {/* <h3>Image thumbnail list</h3> */}
      <div className="d-flex flex-wrap" onClick={changeMainImage}>
        {imageList.map(img => (
          <div
            key={img.id}
            className={
              mainImage.id === img.id
                ? "img-wrapper me-3 mb-3 border border-info border-4 rounded"
                : "img-wrapper me-3 mb-3"
            }
            style={{ cursor: "pointer" }}
          >
            <img
              src={img.download_url}
              className="img-thumbnail rounded"
              alt={`${img.author}#${img.id}`}
              id={img.id}
            />
          </div>
        ))}
      </div>
    </>
  );
}

export default ImageThumbnailList;
