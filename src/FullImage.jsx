import { BsChevronLeft, BsChevronRight } from "react-icons/bs";

function FullImage({ image, handlePrevious, handleNext, imageList }) {
  return (
    <>
      {/* <h3>Full Image</h3> */}
      <div className="d-flex justify-content-center align-items-center justify-content-evenly mt-3 mb-5">
        <div onClick={e => handlePrevious(e)} style={{ cursor: "pointer" }}>
          <BsChevronLeft
            size={70}
            color={imageList[0].id !== image.id ? "#bbb" : "aliceblue"}
          />
        </div>

        <figure className="figure">
          <img
            src={image.download_url}
            className="figure-img img-fluid rounded"
            alt={`${image.author}#${image.id}`}
          />
          <figcaption className="figure-caption text-center">
            <i>
              <span>
                URL :{" "}
                <a href={image.url} target="_blank" rel="noreferrer">
                  {image.url}
                </a>
              </span>
            </i>
            {/* {`${image.author}#${image.id}`} */}
          </figcaption>
        </figure>
        <div onClick={handleNext} style={{ cursor: "pointer" }}>
          <BsChevronRight
            size={70}
            color={
              imageList[imageList.length - 1].id !== image.id
                ? "#bbb"
                : "aliceblue"
            }
          />
        </div>
      </div>
    </>
  );
}

export default FullImage;
